
// Typings. (Can be re-generated.)
// !code: imports // !end
// !code: init // !end

declare module '*.json';

// Application.

// Services.
declare module 'feathers-generic';

// !code: more
declare module 'feathers-blob'
declare module 'fs-blob-store'
declare module 'nodemailer'
// !end

// !code: funcs // !end
// !code: end // !end
