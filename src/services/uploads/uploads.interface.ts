
// Define TypeScript interface for service `uploads`. (Can be re-generated.)
// !code: imports // !end
// !code: init // !end

// tslint:disable-next-line:no-empty-interface
export interface UploadBase {
  // !code: interface // !end
}

// tslint:disable-next-line:no-empty-interface
export interface Upload extends UploadBase {
  // !code: more // !end
}

// !code: funcs // !end
// !code: end // !end
