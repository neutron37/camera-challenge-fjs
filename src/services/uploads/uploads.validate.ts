
/* tslint:disable:quotemark */
// Validation definitions for validateSchema hook for service `uploads`. (Can be re-generated.)
import { validateSchema, required } from 'feathers-hooks-common';
import merge from 'lodash.merge';
import ajv from 'ajv';
// !code: imports // !end
// !code: init // !end

// !<DEFAULT> code: set_id_type
// tslint:disable-next-line:no-unused-variable
const ID = 'string';
// !end

let base = merge({},
  // !<DEFAULT> code: base
  {
    title: "Uploads",
    description: "Uploads database.",
    required: [],
    uniqueItemProperties: [],
    properties: {}
  },
  // !end
  // !code: base_more // !end
);
// !code: base_change // !end

let create = merge({},
  base,
  // !code: create_more
  // Initiall schema generated using https://jsonschema.net
  {
    definitions: {},
    type: "object",
    title: "Uploads Schema",
    required: [
      "uri"
    ],
    properties: {
      uri: {
        $id: "#/properties/uri",
        type: "string",
        title: "Image Uri Schema",
        default: "",
        examples: [
            "data:image/png;base64,VE9PTUFOWVNFQ1JFVFM="
        ],
        // See https://stackoverflow.com/a/5885097
        pattern: "^data:image\/(?:jpg|jpeg|png);base64,(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$"
      }
    }
  }
  // !end
);

let update = merge({},
  base,
  // !code: update_more // !end
);

let patch = merge({},
  base,
  // !code: patch_more // !end
);
delete patch.required;
// !code: all_change // !end

let validateCreate = (options?: any) => {
  // !<DEFAULT> code: func_create
  return validateSchema(create, ajv, options);
  // !end
};

let validateUpdate = (options?: any) => {
  // !<DEFAULT> code: func_update
  return validateSchema(update, ajv, options);
  // !end
};

let validatePatch = (options?: any) => {
  // !<DEFAULT> code: func_patch
  return validateSchema(patch, ajv, options);
  // !end
};

let quickValidate = (method: string, data: any, options?: any) => {
  try {
    if (method === 'create') { validateCreate(options)({ type: 'before', method: 'create', data } as any); }
    if (method === 'update') { validateCreate(options)({ type: 'before', method: 'update', data } as any); }
    if (method === 'patch') { validateCreate(options)({ type: 'before', method: 'patch', data } as any); }
  } catch (err) {
    return err;
  }
};
// !code: validate_change // !end

// tslint:disable:trailing-comma
let moduleExports = {
  create,
  update,
  patch,
  validateCreate,
  validateUpdate,
  validatePatch,
  quickValidate,
  // !code: moduleExports // !end
};

// !code: exports // !end
export default moduleExports;

// !code: funcs // !end
// !code: end // !end
