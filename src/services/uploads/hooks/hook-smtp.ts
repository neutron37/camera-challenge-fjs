
// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

import { Hook } from '@feathersjs/feathers';
import { checkContext, getItems, replaceItems } from 'feathers-hooks-common';
import nodemailer from 'nodemailer';
import logger from '../../../logger';

// tslint:disable-next-line:no-unused-variable
export default function (options: any = {}): Hook {

  const feathers = require('@feathersjs/feathers');
  const configuration = require('@feathersjs/configuration');

  // Use the application root and `config/` as the configuration folder
  let app = feathers().configure(configuration());
  let smtpConfig = app.get('smtp_config');
  let uploadsServiceDir = app.get('uploads_service_dir');

  // Return the actual hook.
  return async (context) => {
    // Throw if the hook is being called from an unexpected location.
    checkContext(context, null, ['find', 'get', 'create', 'update', 'patch', 'remove']);

    // Get the authenticated user.
    // tslint:disable-next-line:no-unused-variable
    const { user } = context.params!;
    // Get the record(s) from context.data (before), context.result.data or context.result (after).
    // getItems always returns an array to simplify your processing.
    const records = getItems(context);

    /*
    Modify records and/or context.
     */
    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    let testAccount = await nodemailer.createTestAccount();

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      host: smtpConfig.host,
      port: smtpConfig.port,
      secure: smtpConfig.secure, // true for 465, false for other ports
      auth: {
        user: smtpConfig.user, // generated ethereal user
        pass: smtpConfig.pass // generated ethereal password
      }
    });

    // send mail with defined transport object
    let info = await transporter.sendMail({
      from: '"Test 👻" <' + smtpConfig.user + '>', // sender address
      to: smtpConfig.user, // list of receivers
      subject: 'Hello ✔', // Subject line
      text: 'Hello world', // plain text body
      html: '<b>Hello world</b>', // html body
      attachments: [
        {
          path: uploadsServiceDir + '/' + context.result.id + '.pdf'
        }
      ]
    });

    logger.info('Message sent: %s;', info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

    // Place the modified records back in the context.
    replaceItems(context, records);
    // Best practice: hooks should always return the context.
    return context;
  };
}

// Throw on unrecoverable error.
// tslint:disable-next-line:no-unused-variable
function error(msg: string) {
  throw new Error(msg);
}
