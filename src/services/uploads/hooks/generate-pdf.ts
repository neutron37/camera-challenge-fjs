
// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

import { Hook } from '@feathersjs/feathers';
import { checkContext, getItems, replaceItems } from 'feathers-hooks-common';
import pdfKit from 'pdfkit';
import fs from 'fs';

// tslint:disable-next-line:no-unused-variable
export default function (options: any = {}): Hook {

  const feathers = require('@feathersjs/feathers');
  const configuration = require('@feathersjs/configuration');

  // Use the application root and `config/` as the configuration folder
  let app = feathers().configure(configuration());
  let uploadsServiceDir = app.get('uploads_service_dir');

  // Return the actual hook.
  return async (context) => {

    // Throw if the hook is being called from an unexpected location.
    checkContext(context, null, ['find', 'get', 'create', 'update', 'patch', 'remove']);

    // Get the authenticated user.
    // tslint:disable-next-line:no-unused-variable
    const { user } = context.params!;
    // Get the record(s) from context.data (before), context.result.data or context.result (after).
    // getItems always returns an array to simplify your processing.
    const records = getItems(context);

    /*
    Modify records and/or context.
     */

    const doc = new pdfKit();
    // Pipe its output somewhere, like to a file or HTTP response
    // See below for browser usage
    doc.pipe(fs.createWriteStream(uploadsServiceDir + '/' + context.result.id + '.pdf'));

    // Add an image, constrain it to a given size, and center it vertically and horizontally
    doc.image(uploadsServiceDir + '/' + context.result.id, {
      fit: [250, 300],
    });

    // Finalize PDF file
    doc.end();

    // Place the modified records back in the context.
    replaceItems(context, records);
    // Best practice: hooks should always return the context.
    return context;
  };
}

// Throw on unrecoverable error.
// tslint:disable-next-line:no-unused-variable
function error(msg: string) {
  throw new Error(msg);
}
