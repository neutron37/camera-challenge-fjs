
// Initializes the `uploads` service on path `/uploads`. (Can be re-generated.)
import { App } from '../../app.interface';

import createService from './uploads.class';
import hooks from './uploads.hooks';
// !code: imports
import createServiceOverride from 'feathers-blob';
import createModel from 'fs-blob-store';
// !end
// !code: init // !end

let moduleExports = function (app: App) {

  let paginate = app.get('paginate');
  // !code: func_init
  let blobStorageModel = createModel(app.get('uploads_service_dir'));
  // !end

  let options = {
    paginate,
    // !code: options_more
    Model: blobStorageModel
    // !end
  };
  // !code: options_change // !end

  // Initialize our service with any options it requires
  // !code: extend
  app.use('/uploads', createServiceOverride(options));
  // !end

  // Get our initialized service so that we can register hooks
  const service = app.service('uploads');

  service.hooks(hooks);
  // !code: func_return // !end
};

// !code: exports // !end
export default moduleExports;

// !code: funcs // !end
// !code: end // !end
