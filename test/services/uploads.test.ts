/// <reference types="mocha"/>
import assert from 'assert';
import app from '../../src/app';

describe('\'uploads\' service', () => {
  it('registered the service', () => {
    const service = app.service('uploads');

    assert.ok(service, 'Registered the service');
  });

  it('uploads image as expected', async () => {
    const service = app.service('uploads');
    // The service method call `params`
    const params = {};

    const imageURI = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMAAACdt4HsAAAATlBMVEX///9CwPtIwvut5P3v+v/j9v7z+/+56P6o4v2Z3f2f3/3b8/5byPxz0Pz7/v+/6v7R8P5wz/xRxfuJ2P3e9P7E6/560/xjy/yD1vzU8f6mgzqkAAACpklEQVRYhaVXWaKrIAylokyOaK11/xt9NYBEAfU+zlcrJCSHTBCSBFt7qqRUtF9ZelcCJZ+aF0Iz8fK5dMfbVwQt7x6JMzXGxDeM6t6XTiTFQYW4sWJAnhd6UpRzqiZdIDaGq+Plvk+LGp3V1ULvSzJpROk2FbIKVyvp7NCJC6kaJ56gijkVTUQ/IW/L3ie6as/4WC7fkTUjX9C0+AZqjBiDU0pjf1NfyxNS240nHjrD3/IgXsvFMHm8C/lYftcg8bchalZSg/ECRVQHX4pb/x1qYLLxTgjQeMM/BgUB4f4yuMHPc3lCIB5GF3AKHLiInxAVOKHMn24MSL0HXNtoWOBgwB/LHgMTOPxu/8MAa0K7/SqBUcxAKfUyrTcKKhDbIgc80GhpMGmlbjRo58N0uNLf+a4oznh7956HY6BC8Ey/HxCFKAiVK12L/7Z+gTPdIwU1ROOPTrgDlFofp+DlPpb+k/ZcdaCTkfVMQXtWwHCPGr2tQMJKeueKw16ZnQv9C6PZNwJ5vckLzHjliu+eXCunwoH6JFZmkwoSkRt5bFUU9mjpQ9Kh/oyF5gmx00EypuAhrILQhaewLgQk3sBHjD06uMYbeG/tNQaBdA3pFdhACkL5EgIVHhvKQTJdyqPC45IpSOc0uskXYpTOQUFJYZs/UMDsBSUsaXHw8dA7fEkLiiqPKVu3A3Hz9EU1KOvl0tLjBMI42IunClzWg8ay9e9CfyXl/TzPXLYmwQ+TEYShbSxha2O+hnm0uKYeWlusufLzvDoe8+3YXGPtnYkFiTfi2PlMzfHBEx8w3nRql6bRX3HuUsGAkT3i/G3IYpEhK3/Myx40/ah7U195atTNH7bzx/38B8f5yYNXnj15SPaji+Q/+0j2wxOsyHv6AvIe386XR8//f+a2G6fhIpMHAAAAAElFTkSuQmCC';
    // Create a new message with params that contains our user
    const message = await service.create({
      uri: imageURI,
    }, params);

    assert.equal(message.id, '58f06c42ccc147cb32aebd88eab443bad201bccd2a6070154a83608651ae7b08.png');
    assert.equal(message.size, '825');
    assert.equal(message.uri, imageURI);

    assert.ok(!message.additional);
  });

  it('rejects bad image type as expected', async () => {
    const service = app.service('uploads');
    // The service method call `params`
    const params = {};

    const imageURI = 'data:image/gif;base64,R0lGODlhEwATAPcAAFAKEdataFAKEdataFAKE==';
    // Create a new message with params that contains our user
    const message = await service.create({
      uri: imageURI,
    }, params).catch(error => {
      assert.equal(error.name, 'BadRequest');
      assert.equal(error.message, 'Data does not match schema');
      assert.equal(error.code, '400');
      assert.ok(!error.additional);
    });
  });
});
